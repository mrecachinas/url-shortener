from flask import (
        Blueprint,
        request,
        session,
        jsonify,
        redirect,
        g,
        current_app
)
import pymongo
import memcache
import requests
import random
import string
from urlparse import urlparse

url_shortener_api = Blueprint("url_shortener_api", __name__)

@url_shortener_api.route('/url/<hash_val>')
def get_url(hash_val):
    collection_name = current_app.config.get("DB_COLLECTION_NAME")
    collection = g.db[collection_name]
    url = None
    try:
        url = lookup_hash(collection, hash_val)
        return jsonify({"url": url, "hash": hash_val})
    except TypeError as e:
        return jsonify({"No URL with hash {hash_val}".format(hash_val=hash_val)}), 400
    except Exception as e:
        current_app.logger.exception(e)
        return jsonify({"Exception occurred trying to get to {hash_val}".format(hash_val=hash_val)}), 400

@url_shortener_api.route('/hash')
def get_hash():
    collection_name = current_app.config.get("DB_COLLECTION_NAME")
    collection = g.db[collection_name]
    parsed_url = None
    url = None
    try:
        url = request.args.get('url')
        parsed_url = check_url(url)
    except KeyError as e:
        return jsonify({"error": "No URL provided".format(url=url)}), 400
    except Exception as e:
        current_app.logger.exception(e)        

    if not parsed_url:
        return jsonify({"error": "Not valid URL: {url}".format(url=url)}), 400

    hashed_url = None
    try:
        hashed_url = lookup_url(collection, g.cache, parsed_url)
    except TypeError as e:
        hashed_url = generate_hash(parsed_url)
        _id = insert_hashed_url(collection, g.cache, parsed_url, hashed_url)
    except Exception as e:
        current_app.logger.exception(e)

    return jsonify({"url": url, "parsed": parsed_url, "hash": hashed_url})

def check_url(url):
    tested_url = None
    parsed_url = None
    try:
        parsed_url = urlparse(url)
        tested_url = test_url(url)
    except requests.exceptions.MissingSchema:
        url = "http://" + parsed_url
        parsed_url = urlparse(url)
        tested_url = test_url(url)
    except Exception as e:
        current_app.logger.exception(e)
    finally:
        if (parsed_url.scheme or parsed_url.netloc) and tested_url:
            return tested_url
        return None

def test_url(url):
    req = requests.head(url, allow_redirects=True)
    code = req.status_code
    if code == 200 or code == 300:
        return req.url
    elif code == 400 or code == 500:
        return False

def insert_hashed_url(collection, cache, url, hashed_url):
    obj = cache.get(url)
    if not obj:
        cache.set(url, hashed_url)
        cache.set(hashed_url, url)
    return collection.insert_one({"url": url, "hash": hashed_url})

def lookup(collection, cache, key, val):
    obj = cache.get(val)
    if not obj:
        doc = collection.find_one({key: val})
        cache.set(doc["url"], doc["hash"])
        cache.set(doc["hash"], doc["url"])
        return doc["hash"] if key == "url" else doc["url"]
    else:
        return obj

def lookup_url(collection, cache, url):
    return lookup(collection, cache, 'url', url)

def lookup_hash(collection, cache, hash_val):
    return lookup(collection, cache, 'hash', hash_val)

def generate_hash(url, length=4):
    return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in xrange(length))

@url_shortener_api.before_request
def before_request():
    db = getattr(g, "_database", None)
    if db is None:
        ip = current_app.config.get("DB_IP")
        port = current_app.config.get("DB_PORT")
        dbname = current_app.config.get("DB_NAME")
        g.db = get_db(ip, port, dbname)
    cache = getattr(g, "cache", None)
    if cache is None:
        cacheip = current_app.config.get("CACHE_IP")
        cacheport = current_app.config.get("CACHE_PORT")
        debug = current_app.config.get("DEBUG")
        g.cache = memcache.Client(['{ip}:{port}'.format(ip=cacheip, port=cacheport)], debug=debug)

@url_shortener_api.teardown_request
def teardown_request(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()

def connect_to_database(ip, port):
    return pymongo.MongoClient(ip, port)

def get_db(ip, port, dbname):
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = connect_to_database(ip, port)
    return db[dbname]