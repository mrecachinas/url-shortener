#!/usr/bin/env python
from flask import (
        Flask,
        session,
        g,
        redirect,
        request,
        render_template,
        jsonify,
        url_for,
        abort,
        send_from_directory
)
import os
import requests
import random
import string
import pymongo
import memcache
from urlparse import urlparse
import logging
from logging.handlers import RotatingFileHandler

def create_app(dbip="localhost",
               dbport=27017,
               dbname="url_shortener",
               collection_name="shortened",
               cacheip="localhost",
               cacheport=11211,
               logfile="/tmp/app.log",
               debug=1,
               secret_key="foo"):
    from api.url_shortener import url_shortener_api
    from views.index import index_view
    from views.redirect import redirect_view

    app = Flask(__name__)
    app.config["DB_IP"] = dbip
    app.config["DB_PORT"] = dbport
    app.config["DB_NAME"] = dbname
    app.config["DB_COLLECTION_NAME"] = collection_name
    app.config["CACHE_IP"] = cacheip
    app.config["CACHE_PORT"] = cacheport
    app.config["SECRET_KEY"] = secret_key

    app.register_blueprint(url_shortener_api, url_prefix='/api')
    app.register_blueprint(index_view)
    app.register_blueprint(redirect_view)

    log_level = logging.INFO
    if debug >= 1:
        log_level = logging.DEBUG

    file_handler = RotatingFileHandler(args.log_path, maxBytes=10000, backupCount=1)
    formatter = logging.Formatter(
            "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
    file_handler.setFormatter(formatter)
    file_handler.setLevel(log_level)
    app.logger.addHandler(file_handler)
    return app

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--ip", default="localhost", help="ip on which the webserver will run")
    parser.add_argument("-p", "--port", type=int, default=5000, help="port on which the webserver will run")
    parser.add_argument("-l", "--log-path", default="/tmp/app.log", help="path where the logfile will be written to")
    parser.add_argument("-d", "--db", default="url_shortener", help="database name")
    parser.add_argument("-c", "--collection", default="shortened", help="collection name")
    parser.add_argument("--db-ip", default="localhost", help="ip on which the properties db will run")
    parser.add_argument("--db-port", type=int, default=27017, help="port on which the properties db will run")
    parser.add_argument("--debug", action="count", default=1, help="set the debug level (0 = INFO, 1 = DEBUG)")
    parser.add_argument("--drop", action="store_true", help="Clear the DB on start")
    parser.add_argument("--cache-ip", default="localhost", help="ip on which the memcached instance will run")
    parser.add_argument("--cache-port", type=int, default=11211, help="port on which the memcached instance will run")

    args = parser.parse_args()

    if args.drop:
        mongo_client = pymongo.MongoClient(args.db_ip, args.db_port)
        mongo_client[args.db][args.collection].remove({})
    app = create_app(dbip=args.db_ip,
                     dbport=args.db_port,
                     dbname=args.db,
                     collection_name=args.collection,
                     cacheip=args.cache_ip,
                     cacheport=args.cache_port,
                     logfile=args.log_path,
                     debug=args.debug)
    app.run(host=args.ip,
            port=args.port,
            debug=True)
