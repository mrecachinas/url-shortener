document.getElementById("submit").onclick = function() {
    var url = document.getElementById("url").value;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/hash?url=" + url);
    xhr.send(null);
    var error = document.getElementById("error");
    var output = document.getElementById("output");
    xhr.onreadystatechange = function () {
        var DONE = 4;
        var OK = 200;
        if (xhr.readyState === DONE) {
            var res = JSON.parse(xhr.responseText);
            error.innerText  = "";
            output.href      = "";
            output.innerText = "";
            if (xhr.status === OK) {
                output.href = window.location.href + res["hash"];
                output.innerText = window.location.href + res["hash"];
            } else {
                console.log('Error: ' + xhr.status);
                console.log('Error: ' + xhr.responseText);
                error.innerText = res["error"];
            }
        }
    };
}