from flask import (
        render_template,
        session,
        Blueprint
)

index_view = Blueprint('home', __name__)

@index_view.route('/')
def home():
    error = None
    if 'error' in session:
        error = session['error']
        session.pop("error", None)
    return render_template('index.html', error=error)
