from flask import (
        render_template,
        redirect,
        send_from_directory,
        session,
        url_for,
        Blueprint,
        current_app,
        g
)
from api.url_shortener import lookup_hash
import pymongo
import memcache

redirect_view = Blueprint('redirect', __name__)

@redirect_view.route('/<hash_val>')
def get_url(hash_val):
    if hash_val == "favicon.ico":
        return send_from_directory('static', filename='favicon.ico')

    url = '/'
    collection_name = current_app.config.get("DB_COLLECTION_NAME")
    collection = g.db[collection_name]
    try:
        url = lookup_hash(collection, g.cache, hash_val)
    except TypeError as e:
        session['error'] = "No URL with hash {hash_val}".format(hash_val=hash_val)
        current_app.logger.exception(e)
    except Exception as e:
        current_app.logger.exception(e)
    finally:
        return redirect(url)

@redirect_view.before_request
def before_request():
    db = getattr(g, "_database", None)
    if db is None:
        ip = current_app.config.get("DB_IP")
        port = current_app.config.get("DB_PORT")
        dbname = current_app.config.get("DB_NAME")
        g.db = get_db(ip, port, dbname)
    cache = getattr(g, "cache", None)
    if cache is None:
        cacheip = current_app.config.get("CACHE_IP")
        cacheport = current_app.config.get("CACHE_PORT")
        debug = current_app.config.get("DEBUG")
        g.cache = memcache.Client(['{ip}:{port}'.format(ip=cacheip, port=cacheport)], debug=debug)

@redirect_view.teardown_request
def teardown_request(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()

def connect_to_database(ip, port):
    return pymongo.MongoClient(ip, port)

def get_db(ip, port, dbname):
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = connect_to_database(ip, port)
    return db[dbname]