# URL Shortener

## Tech Stack

- Python 2.7
- Flask 0.11
- MongoDB 3.3
- Docker 1.12
- Docker-Compose 1.8

## Installation

The easiest way to run this application is to ensure [docker](http://docker.com) and docker-compose are installed and run

```
docker-compose up
```

## TODO

- [ ] Fix URL hashing
    - [ ] It's only hashing the base URL (e.g., "http://facebook.com/foobar" => "http://facebook.com")
    - [ ] It disallows protocol-less URLs (e.g., "facebook.com")
- [x] Add caching layer
- [ ] Edit Docker-Compose to shard Mongo and add replicas
- [ ] Kubernetes-ify or Docker Swarm this app
